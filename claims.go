package _core

import (
	"github.com/dgrijalva/jwt-go"
	"gopkg.in/mgo.v2/bson"
)

type Claims struct {
	Id                 bson.ObjectId `json:"id"`
	Email              string        `json:"email"`
	FirstName          string        `json:"firstName"`
	LastName           string        `json:"lastName"`
	DeviceId           string        `json:"deviceId"`
	Exp                int64         `json:"exp"`
	Level              int           `json:"level"`
	jwt.StandardClaims `json:"standardClaims"`
}

func (claims Claims) Authorized(action string) bool {
	return true
}
