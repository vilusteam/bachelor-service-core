package _core

import (
	"errors"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"os"
	"strconv"
	"time"
)

const database = "db"

var mongoDatabase *mgo.Database
var mongoSession *mgo.Session

func initDatabase() {
	fmt.Println("Connecting to MongoDB!")
	var err1 error
	port := "27017"
	if os.Getenv("MONGO_DB_PORT") != "" {
		port = os.Getenv("MONGO_DB_PORT")
	}
	host := "mongodb"
	if os.Getenv("MONGO_DB_HOST") != "" {
		host = os.Getenv("MONGO_DB_HOST")
	}
	mongoSession, err1 = mgo.Dial(os.Getenv("MONGO_INITDB_ROOT_USERNAME") + ":" + os.Getenv("MONGO_INITDB_ROOT_PASSWORD") + "@" + host + ":" + port)
	if err1 != nil {
		panic(err1)
	}

	//Ping every minute to keep session alive
	ticker := time.NewTicker(time.Minute)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := mongoSession.Ping()
				if err != nil {
					fmt.Println("Failed to ping MongoDB")
					ticker.Stop()
					break
				}
			}
		}
	}()

}

func GetMongoCollection(collection string) *mgo.Collection {
	return getMongoDB().C(collection)
}

func getMongoDB() *mgo.Database {
	if mongoDatabase == nil {
		if getMongoSession() == nil {
			panic("No Mongo Session Instace")
		}

		mongoDatabase = getMongoSession().DB(database)
	}
	return mongoDatabase
}

func QueryPaging(query *mgo.Query, r *http.Request) (int, *mgo.Query, error) {
	limit := r.URL.Query().Get("limit")
	page := r.URL.Query().Get("page")

	totalCount, err := query.Count()
	if err != nil {
		return 0, nil, err
	}

	//Dealing with Pagination
	if len(limit) != 0 {
		limitNumber, err := strconv.Atoi(limit)
		if err != nil {
			return 0, nil, err
		}
		if len(page) != 0 {
			pageNumber, err := strconv.Atoi(page)
			if err != nil {
				return 0, nil, err
			}
			if pageNumber <= 0 {
				return 0, nil, errors.New("invalid page number")
			}
			query.Skip(limitNumber * (pageNumber - 1))
		}
		query.Limit(limitNumber)
	}

	return totalCount, query, nil
}

func getMongoSession() *mgo.Session {
	if mongoSession == nil {
		initDatabase()
	}

	return mongoSession
}

func ParseObjectID(id string) (bson.ObjectId, bool) {
	var ObjectID bson.ObjectId
	if !bson.IsObjectIdHex(id) {
		return "", false
	}
	ObjectID = bson.ObjectIdHex(id)

	return ObjectID, ObjectID.Valid()
}
