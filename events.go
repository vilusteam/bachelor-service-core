package _core

import (
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

type EventHandler struct {
	conn          *websocket.Conn
	Events        map[int]func([]byte)
	connectionMux sync.Mutex
}

func (handler *EventHandler) checkConnection() {
	//this function is synced
	handler.connectionMux.Lock()

	defer func() {
		handler.connectionMux.Unlock()
	}()

	if handler.conn != nil { //if connection exists -> return
		return
	}
	var err error
	host := "ws://" + os.Getenv("AUTH_SERVICE_DOMAIN") + "events"
	fmt.Println(host)
	handler.conn, _, err = websocket.DefaultDialer.Dial(host, nil)
	defer func() {
		//unlink connection
		handler.conn = nil
	}()

	if err != nil {
		log.Println("dial:", err)
		return
	}

	defer func() {
		//disconnect connection
		handler.conn.Close()
	}()
	for {
		mt, message, err := handler.conn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			return
		}

		if val, ok := handler.Events[mt]; ok {
			val(message)
		}
	}
}

func (handler *EventHandler) Listen() {
	//connection checker
	go func() {
		for {
			time.Sleep(2 * time.Second)
			go handler.checkConnection()
		}
	}()

	//connection pinger
	go func() {
		for {
			time.Sleep(10 * time.Second)
			if handler.conn != nil {
				handler.conn.WriteMessage(1, []byte("ping"))
			}
		}
	}()
}

func (handler *EventHandler) RegisterEvent(mt int, function func([]byte)) {
	if _, ok := handler.Events[mt]; ok {
		fmt.Println("Event of TYPE" + strconv.Itoa(mt) + " already registered. Overiding...")
	}
	handler.Events[mt] = function
}
