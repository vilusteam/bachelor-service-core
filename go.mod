module bitbucket.org/vilusteam/bachelor-service-core

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/julienschmidt/httprouter v1.3.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
