package _core

import (
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
)

type JWTManager struct {
	revoked map[string]map[string]int64
}

var jwtManager *JWTManager

const RevokeTokenEvent = 2

func GetJWTManager() *JWTManager {
	if jwtManager == nil {
		jwtManager = &JWTManager{revoked: map[string]map[string]int64{}}
	}
	return jwtManager
}

func (manager *JWTManager) Revoke(request RevokeRequest) {
	if manager.revoked[string(request.UserId)] == nil {
		manager.revoked[string(request.UserId)] = map[string]int64{}
	}
	manager.revoked[string(request.UserId)][request.DeviceId] = request.Time
}

func (manager *JWTManager) ClearOld(checkTime int64) {
	for indexUser, userTimes := range manager.revoked {
		for indexTime, time := range userTimes {
			if checkTime > time {
				delete(manager.revoked[indexUser], indexTime)
			}
		}
	}
}

func (manager *JWTManager) Check(claims Claims) bool {
	userId := string(claims.Id)
	userTimes, ok := manager.revoked[userId]
	if ok {
		time, ok := userTimes[claims.DeviceId]
		if ok {
			if claims.Exp <= time {
				return false //tokens were revoked
			}
		}
	}
	return true
}

type RevokeRequest struct {
	UserId bson.ObjectId
	DeviceId string
	Time   int64
}

func revokeToken(bytes []byte) {
	var request RevokeRequest
	err := json.Unmarshal(bytes, &request)
	if err == nil {
		GetJWTManager().Revoke(request)
	}
}

func RegisterJWTEventListener(handler *EventHandler) {
	handler.RegisterEvent(RevokeTokenEvent, revokeToken)
}
