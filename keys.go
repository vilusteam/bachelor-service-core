package _core

import (
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"regexp"
	"runtime/debug"
	"strings"
)

type PublicKey struct {
	Id      int
	Content *rsa.PublicKey
}

const UpdateKeysEvent = 1

var keys []PublicKey

func getKeyById(Id int) (*rsa.PublicKey, bool) {
	for _, element := range keys {
		if element.Id == Id {
			return element.Content, true
		}
	}

	return nil, false
}

func parseKeys(bytes []byte) {
	err := json.Unmarshal(bytes, &keys)
	if err != nil {
		keys = nil
	}
}

func RegisterKeyEventListener(handler *EventHandler) {
	handler.RegisterEvent(UpdateKeysEvent, parseKeys)
}

func ValidateClaims(tokenString string, VerifyKey *rsa.PublicKey) *Claims{
	verifiedToken, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return VerifyKey, nil
	})
	if err != nil {
		return nil
	}

	body := strings.Split(verifiedToken.Raw, ".")[1]
	decoded, err := jwt.DecodeSegment(body)
	if err != nil{
		return nil
	}

	var claims *Claims
	err = json.Unmarshal(decoded, &claims)
	if err != nil{
		return nil
	}


	if !GetJWTManager().Check(*claims){
		return nil
	}

	return claims
}

func GetClaims(r *http.Request) (claim *Claims) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(string(debug.Stack()))
			claim = nil
		}
	}()

	authHeader := r.Header.Get("Authorization")
	authRegxp := regexp.MustCompile("^Bearer (.*)$")
	tokenString := authRegxp.ReplaceAllString(authHeader, "$1")

	Parser := new(jwt.Parser)
	token, _, err := Parser.ParseUnverified(tokenString, jwt.MapClaims{})
	if err != nil {
		return nil
	}

	keyId, ok := token.Header["kid"].(float64)
	if !ok {
		return nil
	}

	VerifyKey, ok := getKeyById(int(keyId))
	if !ok {
		return nil
	}

	return ValidateClaims(tokenString, VerifyKey)
}