package _core

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type MyHandle func(*http.Request, httprouter.Params)


//Handler for REST functions
func HttpHandler(handle MyHandle) (httprouter.Handle) {
	return func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

		//caches all responses and error throws
		defer writeResponse(writer)

		//executes actual REST method
		handle(request, params)

		var empty interface{}
		SendResponse(empty)
	}
}
